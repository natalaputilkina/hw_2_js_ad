/*Теоретичне питання

Наведіть кілька прикладів, коли доречно використовувати в коді конструкцію try...catch.
try...catch використовується якщо необхідно обробити помилки у коді, але доречно використовувати на ті помилки, які можна спрогнозувати і вкаати программі як далі необідно виконувати код.
Наприклад, коли код працює, але з сервера надходять не корректні дані чи не корректні данні були введені користувачем при заповненні інформації.

*/

// Завдання

// Виведіть цей масив на екран у вигляді списку (тег ul – список має бути згенерований за допомогою Javascript).
// На сторінці повинен знаходитись div з id="root", куди і потрібно буде додати цей список (схоже завдання виконувалось в модулі basic).
// Перед додаванням об'єкта на сторінку потрібно перевірити його на коректність (в об'єкті повинні міститися всі три властивості - author, name, price). Якщо якоїсь із цих властивостей немає, в консолі має висвітитися помилка із зазначенням - якої властивості немає в об'єкті.
// Ті елементи масиву, які не є коректними за умовами попереднього пункту, не повинні з'явитися на сторінці.

const books = [
    { 
      author: "Люсі Фолі",
      name: "Список запрошених",
      price: 70 
    }, 
    {
     author: "Сюзанна Кларк",
     name: "Джонатан Стрейндж і м-р Норрелл",
    }, 
    { 
      name: "Дизайн. Книга для недизайнерів.",
      price: 70
    }, 
    { 
      author: "Алан Мур",
      name: "Неономікон",
      price: 70
    }, 
    {
     author: "Террі Пратчетт",
     name: "Рухомі картинки",
     price: 40
    },
    {
     author: "Анґус Гайленд",
     name: "Коти в мистецтві",
    }
];

let div = document.createElement('div');
div.id = 'root';
document.body.append(div);

function sendBooks (books) {
    let ul = document.createElement('ul');

    for (let book of books) {

      let li = document.createElement('li');
      li.textContent = `book autor: ${book.author}; book name: ${book.name}; book price: ${book.price}`;
      try{
        if (book.author && book.name && book.price) {
          ul.append(li);
        }else{
          throw new Error(`No ${!book.author ? 'author'  : !book.name ? 'name' : 'price'}`)
        }
      }
      catch (e) {
        console.log(e.message);
      }
    
      
    }
   return ul
}

let sendBooksAdd = sendBooks(books);
document.getElementById('root').append(sendBooksAdd);




